const yargs = require('yargs');
const geocode = require('./geocode/geocode.js')

const address_options = {
    demand: true,
    alias: 'address',
    describe: 'Address to fetch weather for',
    string: true
};

const argv = yargs.options({
        a: address_options
    })
    .help().alias('help', 'h').argv

geocode.geocodeAddress(argv.address,

    (errorMessage, results) => {
        if (errorMessage) {
            console.log(errorMessage);
        } else {
            console.log(JSON.stringify(results, undefined, 2));
        }
    });